xfce4-timer-plugin (1.7.1-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Debian Janitor <janitor@jelmer.uk>  Tue, 24 Aug 2021 14:08:47 -0000

xfce4-timer-plugin (1.7.1-1) unstable; urgency=medium

  * Team upload.
  * d/watch: Update for mirrorbit and use uscan special strings.
  * New upstream version 1.7.1.
  * d/compat, d/control:
    - Drop d/compat in favor of debhelper-compat, bump to 13.
  * d/control:
    - Drop B-D on libtool and libxml-parser-perl.
    - Set M-A: same.
    - R³: no.
    - Build-Depend on xfce4-dev-tools.
    - Remove Lionel Le Folgoc from uploaders, thanks!
  * d/copyright: Update Source field.
  * d/rules: Work around m4/ missing during autoreconf.
  * Update Standards-Version to 4.5.0.
  * Trim trailing whitespace in previous changelog entries.

 -- Unit 193 <unit193@debian.org>  Wed, 29 Jul 2020 19:52:04 -0400

xfce4-timer-plugin (1.7.0-1) unstable; urgency=medium

  [ Yves-Alexis Perez ]
  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
  * New upstream version 1.7.0
  * d/p/01_improve-timer-stop.patch: refresh for new release
  * d/p/02_fix-format-string removed, included upstream
  * d/rules: stop touching POTFILES.skip
  * d/control: drop build-dep on dh-autoreconf and xfce4-dev-tools
  * run wrap-and-sort
  * d/control: replace gtk2 build-dep by gtk3
  * d/control: replace libxfce4ui-1-dev by libxfce4ui-2-dev
  * d/control: replace xfce4-panel-dev build-dep by libxfce4panel-2.0-dev
  * d/p/01_improve-timer-stop removed, actually included upstream
  * d/control: update standards version to 4.1.3
  * d/rules: remove libxfcetimer.la file after install

  [ Unit 193 ]
  * d/copyright: Update to DEP-5 format.
  * d/watch: Use https in upstream URL.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 28 Mar 2018 22:59:06 +0200

xfce4-timer-plugin (1.6.0-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Jackson Doak ]
  * New upstream release
  * Refresh patches
  * debian/control:
    - Add build-depends on dh-autoreconf, xfce4-dev-tools
    - Bump standards-version to 3.9.5
  * debian/rules: Use dh_autoreconf. Closes: #554976

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 30 Sep 2014 21:50:27 +0200

xfce4-timer-plugin (1.5.0-1) unstable; urgency=low

  * New upstream release.                                       closes: #709338
  * debian/patches:
    - 01_improve-timer-stop refreshed.
    - 02_fix-format-string added, fix a format string issue.
  * debian/rules:
    - enable all hardening flags.
  * debian/control:
    - update build-deps, switch to libxfce4ui-dev.
    - update standards version to 3.9.3.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 22 May 2013 21:42:15 +0200

xfce4-timer-plugin (0.6.3-1) unstable; urgency=low

  [ Lionel Le Folgoc ]
  * New upstream release, including:
    - updated translations
    - fixed response on left click mouse events.                    lp: #800659

  [ Yves-Alexis Perez ]
  * debian/rules:
    - use debhelper 9 hardening support.
    - don't run xdt-autogen, not needed anymore.
  * debian/compat bumped to 9.
  * debian/control:
    - drop xfce4-dev-tools build-dep.
    - update debhelper build-dep to 9 for hardening support.
    - add dpkg-dev 1.16.1 build-dep for hardening support.

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Mon, 23 Jan 2012 17:22:23 +0100

xfce4-timer-plugin (0.6.2-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - 02_Fix-segfault-when-creating-plugin-in-4.8-panel.patch: dropped,
      included upstream.
    - series: refreshed.

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Sun, 04 Sep 2011 22:46:46 +0200

xfce4-timer-plugin (0.6.1-4) unstable; urgency=low

  [ Lionel Le Folgoc ]
  * debian/control: change Section to 'xfce'.
  * debian/rules:
    - pick {C,LD}FLAGS from dpkg-buildflags.
    - add -O1, -z,defs and --as-needed to LDFLAGS.
    - add hardening flags to {C,LD}FLAGS.
    - add .pc dir to po/POTFILES.skip, and remove it at clean time.
    - replace dh-autoreconf addon by xdt-autogen call.
  * debian/patches:
    - 01_improve-timer-stop.patch: drop the POTFILES.skip part, done in
      debian/rules now.
    - 02_Fix-segfault-when-creating-plugin-in-4.8-panel.patch: make the
      plugin work with Xfce 4.8.
    - series: refreshed.
  * debian/control:
    - remove Simon and Emanuele from uploaders, thanks to them.
    - add build-dep on hardening-includes.
    - bump debhelper b-dep to (>= 7.0.50~) for the overrides.
    - bump xfce4-panel-dev b-dep to (>= 4.8.0).
    - add build-dep on xfce4-dev-tools and libtool.

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.2.
    - drop cdbs build-dep.

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Wed, 20 Apr 2011 10:01:15 +0200

xfce4-timer-plugin (0.6.1-3) unstable; urgency=low

  * debian/patches/01_improve-timer-stop.patch: refreshed, added .pc directory
    to POTFILES.skip, fixes FTBFS.                              closes: #560662
  * debian/control: added myself to uploaders.

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Sat, 10 Apr 2010 21:12:05 +0100

xfce4-timer-plugin (0.6.1-2) unstable; urgency=low

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Yves-Alexis Perez ]
  * debian/watch edited to track Xfce archive reorganisation.
  * convert package to 3.0 (quilt) format.
  * debian/patches:
    - 01_improve-timer-stop added.                     closes: #554601, #554597
  * debian/rules:
    - switch to debhelper 7.
    - add -Wl,-z,defs -Wl,--as-needed to LDFLAGS.
  * debian/control:
    - update standards version to 3.8.3.
    - update debhelper build-dep to 7.
    - add build-dep on autotools-dev to refresh config.{guess,sub}.
  * debian/compat bumped to 7.
  * debian/docs: add some docs to the package.

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 06 Nov 2009 08:37:27 +0100

xfce4-timer-plugin (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - added build-dep on intltool, pkg-config, libglib2.0-dev,
      libgtk2.0-dev, libxfcegui4-dev.
    - update build-dep on xfce4-panel-dev to match Xfce 4.4.
    - remove Rudy and Martin from Uploaders:.
    - updated standards version to 3.8.0.
  * debian/copyright:
    - update dates.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 22 Feb 2009 20:07:50 +0100

xfce4-timer-plugin (0.6-1) unstable; urgency=low

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Thu, 27 Dec 2007 22:59:08 +0100

xfce4-timer-plugin (0.5.1-2) unstable; urgency=low

  * debian/patches:
    - 01_implicit-pointer-conversion added.                     closes: #456316

 -- Yves-Alexis <corsac@molly.corsac.net>  Thu, 20 Dec 2007 15:58:55 +0000

xfce4-timer-plugin (0.5.1-1) unstable; urgency=low

  [ Yves-Alexis Perez ]
  * Initial release.                                            closes: #407198

  [ Simon Huggins ]
  * Add Vcs-* headers to debian/control

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 08 Dec 2007 22:43:39 +0100
